#!/bin/bash
set -e

if [ "$1" = 'install' ]; then

    node -v
    npm -v
    php -v


    rm -rf dist
    cp -r backend/* dist/
    cd dist

    composer config -g github-oauth.github.com de749b0365510317a897a451d4a6db9711606f82
    composer install --no-interaction --prefer-dist

    # устанавливаем параметры
    cp ../config/parameters.yml.def app/config/parameters.yml

    # создаем папку для загрузки файлов

    if [ ! -d "web/uploads" ]; then
        mkdir web/uploads
    fi


    cd ../frontend/

    npm install
    bower install --allow-root
    #rm -r ../dist/web/{assets,assets,kityformula-editor,scripts,styles,template,index.html}
    gulp build

    # устанавливаем параметры
    cp ../config/config_client.js.def ../dist/web/assets/config_client.js

elif [ "$1" = "build" ]; then
    echo  "build"
elif [ "$1" = "release" ]; then
    echo "Релиз"
    cd /var/www
    cp -r dist/* release/
fi

#exec "$@"