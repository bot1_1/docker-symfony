FROM debian:jessie
MAINTAINER Vladimir Daron <v@webant.ru>

RUN apt-get update && \
    apt-get install -y \
        git \
        curl \
        php5-cli \
        php5-json \
        php5-intl \
        php5-gd

RUN curl -sL https://deb.nodesource.com/setup_5.x | bash -
RUN apt-get install -y \
        nodejs

RUN npm install -g bower gulp gulp-cli

# устанавливаем php зависимости
RUN curl -sL https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

WORKDIR /var/www/

ADD backend /var/www/dist
ADD frontend /var/www/frontend
ADD config /var/www/config

#--------------   BACKEND  --------------
WORKDIR /var/www/dist

# устанавливаем зависимости
RUN composer config -g github-oauth.github.com de749b0365510317a897a451d4a6db9711606f82
RUN composer install --no-interaction --prefer-dist

# устанавливаем параметры
RUN ls /var/www/config/
COPY config/parameters.yml.def app/config/parameters.yml
#--------------     END    ---------------



#--------------  FRONTEND  ---------------
WORKDIR /var/www/frontend/

# устанавливаем зависимости
RUN npm install
RUN bower install --allow-root
#RUN rm -r ../dist/web/{assets,assets,kityformula-editor,scripts,styles,template,index.html}
# собираем frontend
RUN gulp build

# устанавливаем параметры
COPY config/config_client.js.def ../dist/web/assets/config_client.js
#--------------     END    ---------------

COPY start.sh /start.sh
RUN chmod +x /start.sh


VOLUME ["/var/www/release"]

ENTRYPOINT ["/start.sh"]
CMD ["echo", "kEGE 2016"]